variable "location" {
  type = string
  description = "Azure region to deploy in."
  default = "uksouth"

  validation {
      condition = contains(["uksouth","ukwest"], var.location)
      error_message = "Valid locations are uksouth and ukwest."
  }
}

variable "app_name" {
  type = string
  description = "Application name. Used in VM names."
}

variable "role_name" {
  type = string
  description = "Application role name (e.g. web, db). Used in VM snames."
}

variable "vm_count" {
  type = number
  description = "Number of VMs to deploy."
}

variable "virtual_network_id" {
  type = string
  description = "Resource ID of virtual network to join."
}

variable "vm_size" {
    type = string
    description = "VM Size."

    validation {
        condition = contains(["Standard_B2s"], var.vm_size)
        error_message = "VM Size must be one of: Standard_B2s."
    }
}